_~/.newsboat/config_
```bash
include "~/software/dot-files/newsboat/colors"
```
![newsboat](screenshots/newsboat.png)

--------------------------------------------
[mutt][1] or [neomutt?][0] mutt is fine.
```bash
$ mkdir ~/software/mutt-header_cache # speedy imap w/ header_cache
```

also [nice.][2]
_~/.mutt/muttrc_
```bash
source "~/software/dot-files/mutt/layout.muttrc"
source "~/software/dot-files/mutt/colors.muttrc"
```
![git](screenshots/mutt.png)


--------------------------------------------
_~/.gitconfig_
```bash
[include]
    path = ~/software/dot-files/git/gitconfig
```
![git](screenshots/git.png)

[0]: https://github.com/neomutt/neomutt/issues/1075
[1]: https://gitlab.com/muttmua/mutt/issues/12
[2]: https://github.com/dracula/mutt
